import { NgModule } from '@angular/core'; 
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthRoutingModule } from './pages/auth/auth-routing.module';

const routes: Routes = [
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule) },
  { path: 'contas', loadChildren: () =>  import("./pages/contas/contas.module").then(m => m.ContasModule)},  
  { path: 'auth', loadChildren: () =>  import("./pages/auth/auth.module").then(m => m.AuthModule) }
];

@NgModule({
  imports: [
    AuthRoutingModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
