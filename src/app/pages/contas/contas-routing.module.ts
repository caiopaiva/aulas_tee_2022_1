import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastroPage } from './cadastro/cadastro.page';
import { ReceberPage } from './receber/receber.page';
import { RelatorioPage } from './relatorio/relatorio.page';
import { PagarPage } from './pagar/pagar.page';
import { ListaPage } from './lista/lista.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '', children: [
      {path: 'pagar', component: ListaPage },
      {path: 'receber', component: ListaPage },
      {path: 'cadastro', component: CadastroPage },
      {path: 'relatorio', component: RelatorioPage },
      {path: 'lista', component: ListaPage}
    ]
  },
  {
    // path: 'lista',
    // loadChildren: () => import('./lista/lista.module').then( m => m.ListaPageModule)
  }

];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PagarPage,
    ListaPage,
    ReceberPage,
    CadastroPage,
    RelatorioPage
  ], 
  exports: [RouterModule]
})
export class ContasRoutingModule { }
