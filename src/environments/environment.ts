// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDF0lje-vEa-niQ8jhAbx729vnyJ7q-o0o',
    authDomain: 'controle-if19.firebaseapp.com',
    projectId: 'controle-if19',
    storageBucket: 'controle-if19.appspot.com',
    messagingSenderId: '473646060474',
    appId: '1:473646060474:web:c3627f4187d887313d8383',
    measurementId: 'G-M01JK4VTH5'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
