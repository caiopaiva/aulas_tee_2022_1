import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContasRoutingModule } from './contas-routing.module';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [],
  imports: [
    // FormsModule,
    // IonicModule,
    CommonModule,
    ContasRoutingModule
  ]
})
export class ContasModule { }
